<?php

use Illuminate\Support\Facades\Route;
use Superius\OmniSanctum\Http\Controllers\SanctumTokenController;
use Superius\OmniSanctum\Http\Controllers\UserController;

Route::middleware('auth.local')->prefix('a2a')->group(function () {
    Route::apiResource('users', UserController::class)->only(['index', 'store']);
    Route::delete('users/{user_id}', [UserController::class, 'destroy']);
    Route::get('/sanctum/tokens/check-if-exists/{token}', [SanctumTokenController::class, 'checkIfExists']);
    Route::post('/sanctum/tokens/create', [SanctumTokenController::class, 'create']);
    Route::delete('/sanctum/tokens/delete/{user_id}', [SanctumTokenController::class, 'delete']);
});
