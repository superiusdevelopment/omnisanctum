<?php

namespace Superius\OmniSanctum\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Superius\OmniSanctum\Requests\StoreUserRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $users = User::all(['id', 'name', 'url'])->sortBy('name')->map(function ($user) {
            return [
                'id' => $user->id,
                'name' => $user->name,
                'url' => $user->url,
                'has_token' => $user->tokens()->exists(),
            ];
        })->values();

        return response()->json(['data' => $users->toArray()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Superius\OmniSanctum\Requests\StoreUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreUserRequest $request): JsonResponse
    {
        $user = User::query()->create($request->validated());

        return response()->json(['data' => $user], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(string $user_id): JsonResponse
    {
        $user = User::query()->findOrFail($user_id);
        $user->tokens()->delete();
        $user->delete();

        return response()->json([]);
    }
}
