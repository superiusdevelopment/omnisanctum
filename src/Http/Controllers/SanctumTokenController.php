<?php

namespace Superius\OmniSanctum\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Laravel\Sanctum\PersonalAccessToken;
use Superius\OmniSanctum\Requests\CreateSanctumTokenRequest;

class SanctumTokenController extends Controller
{
    public function create(CreateSanctumTokenRequest $request): JsonResponse
    {
        /** @var User $user */
        $user = User::query()->findOrFail($request->get('user_id'));

        $token = $user->createToken($request->get('token_name'));

        return response()->json(['token' => $token->plainTextToken], 201);
    }

    public function checkIfExists(string $token): JsonResponse
    {
        /** @var PersonalAccessToken|null $personalAccessToken */
        $personalAccessToken = PersonalAccessToken::findToken($token);

        return response()->json([
            'exists' => $tokenExists = (bool)$personalAccessToken,
            'user_id' => $tokenExists ? $personalAccessToken?->getAttribute('tokenable_id') : null,
        ]);
    }

    public function delete(string $user_id): JsonResponse
    {
        $user = User::query()->findOrFail($user_id);
        $user->tokens()->delete();

        return response()->json([]);
    }
}
