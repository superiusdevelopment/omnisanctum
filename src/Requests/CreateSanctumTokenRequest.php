<?php

namespace Superius\OmniSanctum\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSanctumTokenRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string,mixed>
     */
    public function rules(): array
    {
        return [
            'user_id' => ['required', 'uuid', 'exists:users,id'],
            'token_name' => ['required', 'string', 'max:255', 'unique:personal_access_tokens,name'],
        ];
    }
}
